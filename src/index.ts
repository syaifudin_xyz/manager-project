import "reflect-metadata";

import { database } from "./database";

import * as express from "express";
import * as cors from "cors";
import * as http from "http";
import * as bodyParser from "body-parser";
import { buildSchema } from "type-graphql";
import { ApolloServer } from "@apollo/server";
import { expressMiddleware } from "@apollo/server/express4";
import { ApolloServerPluginDrainHttpServer } from "@apollo/server/plugin/drainHttpServer";
import { UsersResolvers } from "./users/resolvers";
import Container from "typedi";

(async () => {
  // Initialize datasource
  database
    .initialize()
    .then(() => {
      console.log("Data Source has been initialized!");
    })
    .catch((err) => {
      console.error("Error during Data Source initialization:", err);
    });

  const schema = await buildSchema({
    resolvers: [UsersResolvers],
    container: Container,
    validate: true,
  });

  // Initialize server
  const app = express();
  const httpServer = http.createServer(app);

  // Set up Apollo Server
  const server = new ApolloServer({
    schema,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  await server.start();
  app.use(cors(), bodyParser.json(), expressMiddleware(server));

  httpServer.listen(3000, () => {
    console.log("Server running on http://localhost:3000/");
  });
})();
