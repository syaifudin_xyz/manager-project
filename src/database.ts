import { DataSource } from "typeorm";
import { UserEntity } from "./models/user";
import { ProjectEntity } from "./models/project";
import { LogEntity } from "./models/log";
export const database = new DataSource({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "",
  database: "log_activity_dev",
  synchronize: true,
  logging: true,
  entities: [UserEntity, ProjectEntity, LogEntity],
  subscribers: [],
  migrations: [],
});
