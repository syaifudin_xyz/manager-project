import { Service } from "typedi";
import { UserEntity } from "../models/user";
import * as argon2 from "argon2";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";

@Service()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly repository: Repository<UserEntity>
  ) {}

  save = async (email: string, password: string) => {
    const hashPassword = await argon2.hash(password);

    const newUser: UserEntity = new UserEntity();
    newUser.email = email;
    newUser.password = hashPassword;
    newUser.createdAt = new Date();
    newUser.updatedAt = new Date();

    return await this.repository.save(newUser);
  };

  update = async (id: number, email: string, password: string) => {
    const user = await this.findById(id);
    user.email = email;
    user.password = password;
    return await user.save();
  };

  delete = async (id: number) => {
    return await this.repository.delete({ id });
  };

  findById = async (id: number) => {
    return await this.repository.findOneByOrFail({ id });
  };

  findAll = async () => {
    return await this.repository.find();
  };
}
