import { Arg, Mutation, Query, Resolver } from "type-graphql";

import { Service } from "typedi";
import { UserService } from "./services";
import { User, UserCreateOrUpdateInput } from "./object";

@Service()
@Resolver(User)
export class UsersResolvers {
  constructor(private readonly userService: UserService) {}

  @Query(() => [User])
  async getUsers() {
    return await this.userService.findAll();
  }

  @Query(() => User)
  async getUser(@Arg("id") id: number) {
    return await this.userService.findById(id);
  }

  @Mutation(() => String)
  async deleteUser(@Arg("id") id: number) {
    return await this.userService.delete(id);
  }

  @Mutation(() => User)
  async updateUser(
    @Arg("id") id: number,
    @Arg("data") data: UserCreateOrUpdateInput
  ) {
    return await this.userService.update(id, data.email, data.password);
  }

  @Mutation(() => User)
  async addUser(@Arg("data") data: UserCreateOrUpdateInput) {
    return await this.userService.save(data.email, data.password);
  }
}
