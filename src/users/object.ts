import { Field, ID, InputType, ObjectType } from "type-graphql";

@ObjectType()
export class User {
  @Field(() => ID)
  id: number;
  @Field()
  email: string;
  @Field()
  createdAt: Date;
  @Field()
  updatedAt: Date;
}

@InputType()
export class UserCreateOrUpdateInput {
  @Field()
  email: string;
  @Field()
  password: string;
}
