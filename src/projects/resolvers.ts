import { Arg, Mutation, Query, Resolver } from "type-graphql";

import { Service } from "typedi";
import { Project, ProjectCreateOrUpdateInput } from "./object";
import { ProjectService } from "./services";

@Service()
@Resolver(Project)
export class ProjectResolver {
  constructor(private readonly service: ProjectService) {}

  @Query(() => [Project])
  async getProjects() {
    return await this.service.findAll();
  }

  @Query(() => Project)
  async getProject(@Arg("id") id: number) {
    return await this.service.findById(id);
  }

  @Mutation(() => String)
  async deleteProject(@Arg("id") id: number) {
    return await this.service.delete(id);
  }

  @Mutation(() => Project)
  async addProject(
    @Arg("authorId") authorId: number,
    @Arg("data") data: ProjectCreateOrUpdateInput
  ) {
    return await this.service.save(data, authorId);
  }
}
