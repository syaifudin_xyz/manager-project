import { Service } from "typedi";
import { InjectRepository } from "typeorm-typedi-extensions";
import { Repository } from "typeorm";
import { ProjectEntity } from "../models/project";
import { ProjectCreateOrUpdateInput } from "./object";
import { UserEntity } from "../models/user";

@Service()
export class ProjectService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    @InjectRepository(ProjectEntity)
    private readonly projectRepository: Repository<ProjectEntity>
  ) {}

  save = async (data: ProjectCreateOrUpdateInput, authorId: number) => {
    const user = await this.userRepository.findOneByOrFail({ id: authorId });
    const project = this.projectRepository.create({
      ...data,
      author: user,
    });
    return await this.projectRepository.save(project);
  };

  delete = async (id: number) => {
    return await this.projectRepository.delete({ id });
  };

  findById = async (id: number) => {
    return await this.projectRepository.findOneByOrFail({ id });
  };

  findByUser = async (id: number) => {
    const user = await this.userRepository.findOneByOrFail({ id });
    return await this.projectRepository.findOneByOrFail({ author: !user });
  };
  findAll = async () => {
    return await this.projectRepository.find();
  };
}
