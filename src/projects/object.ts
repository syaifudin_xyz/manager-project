import { Field, ID, InputType, ObjectType } from "type-graphql";

@ObjectType()
export class Project {
  @Field(() => ID)
  id: number;

  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  preferWorktime: string;

  @Field()
  deadline: Date;

  @Field()
  createdAt: Date;

  @Field()
  updatedAt: Date;
}

@InputType()
export class ProjectCreateOrUpdateInput {
  @Field()
  name: string;

  @Field()
  description: string;

  @Field()
  preferWorktime: string;

  @Field()
  deadline: Date;
}
