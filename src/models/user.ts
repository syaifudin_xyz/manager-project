import { Field, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { ProjectEntity } from "./project";

@Entity("users")
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @Column()
  password: string;

  @OneToMany(() => ProjectEntity, (project) => project.author)
  projects: ProjectEntity[];

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
