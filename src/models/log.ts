import { Field, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { UserEntity } from "./user";
import { ProjectEntity } from "./project";

@Entity("logs")
export class LogEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  preferWorktime: string;

  @Column()
  deadline: Date;

  @ManyToOne(() => UserEntity, (user) => user.projects)
  user: UserEntity;

  @ManyToOne(() => ProjectEntity, (project) => project.logs)
  project: UserEntity;

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
