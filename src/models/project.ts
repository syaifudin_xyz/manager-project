import { Field, ID, ObjectType } from "type-graphql";
import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from "typeorm";
import { UserEntity } from "./user";
import { LogEntity } from "./log";

@Entity("projects")
export class ProjectEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  preferWorktime: string;

  @Column()
  deadline: Date;

  @ManyToOne(() => UserEntity, (user) => user.projects)
  author: UserEntity;

  @OneToMany(() => LogEntity, (log) => log.project)
  logs: LogEntity[];

  @Column()
  createdAt: Date;

  @Column()
  updatedAt: Date;
}
